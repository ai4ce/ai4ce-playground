"""
Documentation generation class, used for creating plots.
"""
import matplotlib.pyplot as plt
import numpy as np


def save_spider_chart(labels, intensities, filename):
    # Calculate angles
    num_vars = len(labels)
    angles = np.linspace(0, 2 * np.pi, num_vars, endpoint=False).tolist()

    # The plot is a circle, so we need to "complete the loop"
    # and append the start to the end.
    intensities = np.concatenate((intensities, [intensities[0]]))
    angles += angles[:1]

    ax = plt.subplots(figsize=(6, 6), subplot_kw={"polar": True})[1]
    ax.fill(angles, intensities, color="darkblue", alpha=0.25, label="Research Intensity")

    # Set the ytick labels to be empty
    ax.set_yticklabels([])

    # Set the xtick labels to be our categories
    ax.set_xticks(angles[:-1])
    ax.set_xticklabels(labels)

    # Add a title
    plt.title("Research Intensity in Different Aspects")

    # Add a legend, adjust the position to be more to the right
    ax.legend(loc="lower right", bbox_to_anchor=(1.3, 0))

    # Save the figure as a PNG file
    plt.savefig(filename, dpi=300, bbox_inches="tight")
    plt.show()


# Example usage
labelSamples = np.array(["General\nResearch", "AI", "Space\nEngineering", "Team Interaction"])
intensitySamples = np.array([3, 4, 2, 5])
save_spider_chart(labelSamples, intensitySamples, "spider_chart.png")
