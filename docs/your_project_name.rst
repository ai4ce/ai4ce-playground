your\_project\_name package
===========================

Submodules
----------

your\_project\_name.main module
-------------------------------

.. automodule:: your_project_name.main
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: your_project_name
   :members:
   :undoc-members:
   :show-inheritance:
