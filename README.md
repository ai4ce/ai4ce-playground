# AI4CE Playground

## Setup
To start out you only need to suffice the following requirements:
1. Have Python 3.11 installed and set as your default Python
2. Having Poetry installed, which can be achieved over pip with the command 'pip install poetry'


Initial setup with poetry
For the initial setup, it is sufficient to simply run the two commands below.
Poetry shell will start an instance of a virtual environment of poetry.
Poetry install will install and/or modify the existing environment, taking care of dependencies for you.
```bash
git clone git@gitlab.com:ai4ce/ai4ce-playground.git
cd ai4ce-playground
poetry install
poetry shell

# if you want to start the front end at a specific port: in this case 1122
streamlit run playground/gui/gui.py --server.port 1122
```

# How to add components to the (design) DecisionDB ?

1. open the website of the compenent you want to add
2. start the DecisionDB UI (best to put it side by side)
3. Enter all fields

## Mapping of selected parameters
- `heater power of a battery -> p_in_max` - Why? The heater is a power consumer and can thereby be modelled as power intake
- `temp_ops_min/max` are the maximim limits of given operatable temperatures from the datasheet/website
