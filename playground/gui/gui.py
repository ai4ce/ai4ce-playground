import streamlit as st

from pages import *

st.set_page_config(layout="wide")

st.markdown('''
<style>
.stApp [data-testid="stToolbar"]{
    display:none;
}
</style>
''', unsafe_allow_html=True)


st.title("AI4CE Plaground")
