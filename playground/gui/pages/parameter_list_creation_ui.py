from collections import OrderedDict
from datetime import datetime
from pathlib import Path
import toml
# import tomli_w
from typing import Any, Dict, List

import streamlit as st
from streamlit_tags import st_tags, st_tags_sidebar

from playground.helpers import load_file
from playground.gui.pages.parameter_creation_helpers import add_to_dict, count_elements, filter_key_names, fetch_toml_from_gitlab, filter_values_by_key, run_git_command, sort_dict_alphabetically

GITLAB_URL: str = "https://gitlab.com/ai4ce/ai4ce-playground/-/raw/main/playground/parameter_unit_list.toml?ref_type=heads"


st.set_page_config(
    layout="wide",
    initial_sidebar_state="collapsed",
    menu_items=None
)

# hide_streamlit_style = """
# <style>
#     #root > div:nth-child(1) > div > div > div > div > section > div {padding-top: 2rem;}
# </style>

# """
# # st.title("Test")
# # if st.checkbox('Remove padding'):
# st.markdown(hide_streamlit_style, unsafe_allow_html=True)

if "new_entry" not in st.session_state:
    st.session_state["new_entry"] = {}
if "parameter_type_text_options_list" not in st.session_state:
    st.session_state["parameter_type_text_options_list"] = []
if "available_unit_names" not in st.session_state:
    st.session_state["available_unit_names"] = []
if "available_unit_symbols" not in st.session_state:
    st.session_state["available_unit_symbols"] = []

if "parameter_name" not in st.session_state:
    st.session_state["parameter_name"] = ""
if "current_output" not in st.session_state:
    st.session_state["current_output"] = {}

if "list_gitlab_added_sorted" not in st.session_state:
    st.session_state["list_gitlab_added_sorted"] = {}
if "loaded_param_list" not in st.session_state:
    st.session_state["loaded_param_list"] = {}
if "parameters_from_origin_untouched" not in st.session_state:
    st.session_state["parameters_from_origin_untouched"] = {}


def update_session_state():
    st.session_state["new_entry"][st.session_state["parameter_name"]
                                  ]["unit_name"] = unit_name
    st.session_state["new_entry"][st.session_state["parameter_name"]
                                  ]["unit_symbol"] = unit_symbol
    st.session_state["new_entry"][st.session_state["parameter_name"]
                                  ]["parameter_type"] = parameter_type
    st.session_state["new_entry"][st.session_state["parameter_name"]
                                  ]["parameter_type_text_options"] = st.session_state["parameter_type_text_options_list"]
    st.session_state["new_entry"][st.session_state["parameter_name"]
                                  ]["parameter_description"] = parameter_description
    st.session_state["new_entry"][st.session_state["parameter_name"]
                                  ]["comment"] = comment


def refresh_page():
    # st.experimental_rerun()
    st.session_state["counter"] = 0


new_entry: dict = {}

# =============================================================================
# ======================= Title Section ============================
# =============================================================================
col_title, col_origin, col_load = st.columns([7, 1, 1])
with col_title:
    st.title("Parameter Creation UI")

with col_origin:
    st.write("")
    PARAMETER_LIST_ORIGIN = st.selectbox(
        "Parameter List Oringin", options=["gitlab", "local"])

with col_load:
    st.write("")
    st.write("")
    st.write("")
    if st.button("Load Parameter List"):
        if PARAMETER_LIST_ORIGIN == "gitlab":
            list_params: dict = fetch_toml_from_gitlab(url=GITLAB_URL)
        elif PARAMETER_LIST_ORIGIN == "local":
            list_params: dict = toml.load(
                "playground/parameter_unit_list.toml")

        st.session_state["loaded_param_list"] = list_params  # maybe obsolete
        st.session_state["parameters_from_origin_untouched"] = list_params
        pass

        st.session_state["available_unit_names"] = filter_values_by_key(
            list_params, key="unit_name")
        st.session_state["available_unit_symbols"] = filter_values_by_key(
            list_params, key="unit_symbol")

# =============================================================================

col1, col2 = st.columns([1, 1])
# loaded_param_list: dict = st.session_state["parameters_from_origin_untouched"]
parameter_type_options: list = st.session_state[
    "parameters_from_origin_untouched"]["__param_extras__"]["parameter_type_options"]
# information about the parameter list
info: dict = st.session_state["parameters_from_origin_untouched"]["__info__"]

with col1:
    st.session_state["parameter_name"]: str = st.text_input("Parameter Name")

    col_create, col_clear = st.columns([1, 1])
    with col_create:
        if st.button("Create Parameter"):
            st.session_state["new_entry"][st.session_state["parameter_name"]] = {}
    with col_clear:
        if st.button("Clear Parameter"):
            # st.session_state["new_entry"] = {}
            st.rerun()

    unit_name: str = None
    try:
        unit_name: str = st_tags(
            label="Unit Name (eg: kilogram)",
            suggestions=st.session_state["available_unit_names"],
            text=f'eg: {str(st.session_state["available_unit_names"][:5])}',
        )[0]
    except IndexError:
        pass

    unit_symbol: str = None
    try:
        unit_symbol: str = st_tags(
            label="Unit Symbol (eg: kg)",
            suggestions=st.session_state["available_unit_symbols"],
            text=f'eg: {st.session_state["available_unit_symbols"][:5]}',
        )[0]
    except IndexError:
        pass

    parameter_type: str = st.selectbox(
        label="Parameter Type", options=parameter_type_options)

    if parameter_type == "text":
        parameter_type_text_option: str = st.text_input("Parameter Option")
        if st.button("Add Option"):
            st.session_state["parameter_type_text_options_list"].append(
                parameter_type_text_option)

    parameter_description: str = st.text_input(
        label="Parameter Description")
    comment: str = st.text_input(label="Comment")

    if st.session_state["parameter_name"] == "":
        st.warning("Initialise the Paramter first by creating it.")
    else:
        if st.button("Create/Update", on_click=update_session_state(), key="create"):
            update_session_state()

with col2:
    st.markdown(GITLAB_URL)
    st.write('The following is the st.["new_entry"]')
    st.write(st.session_state["new_entry"])

    st.divider()

list_gitlab_added: dict = {}

with col1:
    st.divider()

    col_but_left, col_but_right = st.columns([1, 1])

    with col_but_left:

        if st.button(":one: Update and sort unit list", on_click=refresh_page()):

            st.session_state["parameters_origin_added"] = add_to_dict(
                st.session_state["parameters_from_origin_untouched"], st.session_state["new_entry"])

            count: int = count_elements(st.session_state["parameters_origin_added"])


            # comparing the number of entries with the original list from the selected origin
            # only if the number changed, the version number and date will be updated
            if count > st.session_state["parameters_from_origin_untouched"]["__info__"]["number_of_entries"]:
                # update last edited
                st.session_state["parameters_origin_added"]["__info__"]["last_edit"] = datetime.now(
                ).isoformat(timespec='minutes')
                # update version number
                major, minor = map(int, str(st.session_state["loaded_param_list"]["__info__"]["version"]).split('.'))
                minor += 1
                st.session_state["parameters_origin_added"]["__info__"]["version"] = float(f"{major}.{minor}")
                pass

            st.session_state["parameters_origin_added"]["__info__"]["number_of_entries"] = count

            st.session_state["list_gitlab_added_sorted"] = sort_dict_alphabetically(
                st.session_state["parameters_origin_added"])

            col2.container(height=1000).write(
                st.session_state["list_gitlab_added_sorted"])

        if st.button(":two: Override parameter_unit_list.toml"):
            with open("playground/parameter_unit_list.toml", 'w') as file:
                toml.dump(st.session_state["list_gitlab_added_sorted"], file)
            col2.container(height=1000).write(
                load_file(Path("playground/parameter_unit_list.toml")))

        if st.button(":three: Push new toml to git"):
            commit_message = f"add {st.session_state['parameter_name']}"

            git_add_command = f"git add playground/parameter_unit_list.toml"
            git_commit_command = f"git commit -m '{commit_message}'"
            git_push_command = f"git push"

            # Run the commands and get the output
            add_output = run_git_command(git_add_command)
            commit_output = run_git_command(git_commit_command)
            push_output = run_git_command(git_push_command)

            # Display the outputs
            st.success(
                f"Add output: {add_output}\nCommit output: {commit_output}\nPush output: {push_output}")

    with col_but_right:
        st.download_button(
            label=":floppy_disk: Download combined TOML file to disk",
            data=toml.dumps(st.session_state["list_gitlab_added_sorted"]),
            file_name="parameter_unit_list.toml",
            mime="text/toml"
        )

        # st.warning("The following is not still WIP")
        if st.button("Export to parameter_unit_list_new.toml"):
            with open("playground/parameter_unit_list_new.toml", 'w') as file:
                toml.dump(st.session_state["list_gitlab_added_sorted"], file)

        if st.button("Show parameter_unit_list.toml"):
            col2.write(
                load_file(Path("playground/parameter_unit_list.toml")))


# col21, col22 = st.columns([1, 1])
# with col22.expander("Added List in GitLab", expanded=False):
#     st.write(st.session_state["list_gitlab_added_sorted"])

# with col21.expander("Whole List in GitLab", expanded=False):
#     st.write(st.session_state["list_gitlab_added_sorted"])
