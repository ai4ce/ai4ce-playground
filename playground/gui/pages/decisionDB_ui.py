import copy
from typing import Dict, Any
from copy import deepcopy
from pathlib import Path
import toml

import httpx
import pandas as pd
import streamlit as st

from playground.helpers import comp_create, load_file

PARAMETER_GITLAB_URL: str = "https://gitlab.com/ai4ce/ai4ce-playground/-/raw/main/playground/parameter_unit_list.toml?ref_type=heads"


st.set_page_config(layout="wide")

parameter_name_examples = {"mass": "kg",
                           "P_consump_max": "W",
                           "T_ops_max": "K",
                           "T_ops_min": "K",
                           "P_consump_min": "W",
                           "T_ops_avg": "K",
                           "P_consump_avg": "W",
                           "depth": "m",
                           "height": "m",
                           "width": "m",
                           }


system_type_examples = [
    "CubeSat",
    "Satellite",
    "Rover",
    "Drone",
    "Space_Station",
    "Neural_Net",
    "Tiny_House",
]

comp_tags = [
    "CubeSat",
        "AODCS",  # new name of sub-system
        # "attitude",  # old name of sub-system
            "accelerometers",
            "control_moment_gyroscopes",
            "deorbiting_devices",
            "earth_horizon_sensors",
            "gnss",
            "gyroscopes",
            "imus",
            "integrated_aodcs",
            "magnetometers",
            "magnetorquers",
            "propellant_tanks",
            "reaction_wheels",
            "star_sensor",
            "sun_sensors",
            "thrusters",

        "CDH",
            "obc",
            "edge_AIs",
            "fpga",
        "COM",
            "antennas",
            "opticals",
            "receivers",
            "sdrs",
            "transceivers",
            "transmitters",
        "EPS",  # new name of sub-system
        # "eps",  # old name of sub-system
            "batteries",
            "fuel_cells",
            "rtgs",
            "solar_panels",
            "tethers",
        "PAYLOAD",
            "cameras",
            "custom_payloads",
        "STRUCTURE",
            "frames",
            "mechanisms",
        "TCS",
            "heat_switches",
            "radiators",
            "coatings",
            "louvres",
            "heaters",
            "multi_layer_insolations"
]


cubesat_shops: list = [
    "satsearch.co",
    "isispace.nl",
    "cubesatshop.com"
]


def fetch_toml_from_gitlab(url: str) -> dict:
    """
    Fetch a TOML file from a public GitLab repository and return its content as a dictionary.

    Args:
        url (str): The URL of the TOML file in the GitLab repository.

    Returns:
        dict: The content of the TOML file as a dictionary.
    """
    response = httpx.get(url)
    # Raise an exception if the GET request is unsuccessful.
    response.raise_for_status()

    toml_content = toml.loads(response.text)
    return toml_content


col_title, col_origin = st.columns([7, 1])
with col_title:
    st.title("Decision Database Import Interface")

with col_origin:
    PARAMETER_LIST_ORIGIN = st.selectbox(
        "Parameter List Oringin", options=["gitlab", "local"])

if PARAMETER_LIST_ORIGIN == "local":
    units: dict = load_file(
        Path("playground/gui/pages/attribute_units_map.json"))
elif PARAMETER_LIST_ORIGIN == "gitlab":
    units: dict = fetch_toml_from_gitlab(
        # url="https://gitlab.com/ai4ce/public-info/-/raw/main/docs/parameter_unit_list.toml?ref_type=heads")
        url=PARAMETER_GITLAB_URL)


def handle_attribute_units(unit_map: dict) -> list:
    """Converts a dictionary of attribute units into a list of formatted strings.

    Args:
        unit_map (dict): A dictionary containing attribute units information.

    Returns:
        list: A list of formatted strings representing the attribute units.

    Example:
        unit_map = {
             'length': {
                 'name': 'meter',
                 'symbol': 'm',
                 'description': 'Unit of length',
                 'comment': 'Standard unit for measuring length'
             },
             'weight': {
                 'name': 'kilogram',
                 'symbol': 'kg',
                 'description': 'Unit of weight',
                 'comment': 'Standard unit for measuring weight'
             }
         }
    """
    units: list = []
    for key, value in unit_map.items():
        if key not in ["__info__", "__param_extras__"]:
            units.append(
                f"{key}, in: {value['unit_name']} [{value['unit_symbol']}] - {value['parameter_description']} - {value['comment']}")
    return units


def find_unit(unit_map: dict, unit_name: str) -> dict:
    """Find a unit in the unit_map based on the given unit_name.

    Args:
        unit_map (dict): A dictionary containing units as keys and their corresponding values.
        unit_name (str): The name of the unit to search for.

    Returns:
        dict: The value associated with the unit_name if found, otherwise None.
    """
    for key, value in unit_map.items():
        if key == unit_name:
            return value
    return None


if "comp_info" not in st.session_state:
    st.session_state["comp_info"] = {}
if "parameters" not in st.session_state:
    st.session_state["parameters"] = {}
if "value_name_value" not in st.session_state:
    st.session_state["value_name_value"] = {}
if "value_name_value_official" not in st.session_state:
    st.session_state["value_name_value_official"] = {}
if "value_name_value_assumed" not in st.session_state:
    st.session_state["value_name_value_assumed"] = {}
if "params_official" not in st.session_state:
    st.session_state["params_official"] = []
if "params_assumed" not in st.session_state:
    st.session_state["params_assumed"] = []
if "params_calculated" not in st.session_state:
    st.session_state["params_calculated"] = []
col_info, col_picture = st.columns([2, 1])

with col_info:
    col1, col2 = st.columns([1, 1])
    with col1:
        name: str = st.text_input(
            "Name of new Compoent", placeholder="Super Battery 50Wh")
        st.session_state["comp_info"]["name"] = name
        st.session_state["comp_info"]["url_picture"] = st.text_input(
            "Picture URL", placeholder="https://picture-url.space/")
        st.session_state["comp_info"]["url_shop"] = st.text_input(
            "Shop URL", placeholder="https://product-url.space/")
        st.session_state["comp_info"]["manufacturer"] = st.text_input(
            "Manufacturer", placeholder="AICE Space Tech")
        st.session_state["comp_info"]["url_datasheet"] = st.text_input(
            "Datasheet URL", placeholder="https://datasheet-url.space/")


    with col2:
        st.session_state["comp_info"]["sys_type"] = st.selectbox("Select System Type",
                                                                 options=system_type_examples,
                                                                 placeholder="Choose a System Type",
                                                                 #  index=None
                                                                 )
        comp_type = st.multiselect(
            "Select Substystem and Comp Tag", options=comp_tags)
        # st.session_state["comp_info"]["tags"] = ', '.join(comp_type)
        st.session_state["comp_info"]["tags"] = comp_type
        st.session_state["comp_info"]["shop"] = st.selectbox(
            "Select Shop", options=cubesat_shops, placeholder="Choose a Shop")
        st.session_state["comp_info"]["url_manufacturer"] = st.text_input(
            "Manufacturer URL", placeholder="https://manufacturer-url.space/")

    st.session_state["comp_info"]["description"] = st.text_area("Description", placeholder="This is a super battery with 50Wh",
                                                                height=100, max_chars=None, key=None)

    table_viz = copy.deepcopy(st.session_state["comp_info"])
    table_viz["tags"] = ', '.join(table_viz["tags"])

    st.dataframe(table_viz, use_container_width=True)

with col_picture:
    if st.session_state["comp_info"]["url_picture"] != "":
        st.image(st.session_state["comp_info"]["url_picture"],
                 caption="Picture of the Component", use_column_width=True)


st.header("Component Parameters")
# st.write(units)
col11, col12, col13, col14 = st.columns([2, 1, 1, 1])
with col11:
    parameter_name_full = st.selectbox(
        # "Select parameter", options=parameter_name_examples.keys(), key="parameter", placeholder="Chosse a Parameter", index=None)
        "Select parameter", options=handle_attribute_units(units), key="parameter")  # , placeholder="Chosse a Parameter", index=0)
    if parameter_name_full is not None:
        parameter_name: str = parameter_name_full.split(',')[0]
with col12:
    if find_unit(units, parameter_name)["parameter_type"] == "numeric_multi":
        parameter_value: int | float = st.number_input(
            "Value", key="value", format="%.7f")
    if find_unit(units, parameter_name)["parameter_type"] == "numeric_range":
        parameter_value_min: int | float = st.number_input(
            "Value", key="value_min", format="%.7f")
        parameter_value_max: int | float = st.number_input(
            "Value", key="value_max", format="%.7f")
    if find_unit(units, parameter_name)["parameter_type"] == "text":
        parameter_value: str = st.selectbox("", options=find_unit(
            units, parameter_name)["parameter_type_text_options"])
    if find_unit(units, parameter_name)["parameter_type"] == "boolean":
        parameter_value: bool = st.selectbox("", options=find_unit(
            units, parameter_name)["parameter_type_text_options"])

with col13:
    st.write("Unit:")
    if parameter_name is not None:
        # st.write(parameter_name_examples[parameter_name])
        st.write(find_unit(units, parameter_name)["unit_symbol"])
        pass
with col14:
    options = ['Official', 'Assumed', 'Calculated']
    parameter_origin = st.radio("What's the origin?", options)
    st.write(f"You selected: {parameter_origin}")

col21, col22 = st.columns([1, 1])

with col21:

    if st.button("Remove Value"):
        st.session_state["parameters"].pop(
            parameter_name, None)
        st.session_state["value_name_value"].pop(
            parameter_name, None)

        if parameter_origin == "Official":
            st.session_state["params_official"].remove(parameter_name)
        elif parameter_origin == "Assumed":
            st.session_state["params_assumed"].remove(parameter_name)
        elif parameter_origin == "Calculated":
            st.session_state["params_calculated"].remove(parameter_name)

with col22:
    if st.button("Add Value"):
        parameter_value_list: list = []
        parameter_value_list.append(parameter_value)
        st.session_state["value_name_value"][parameter_name] = parameter_value_list
        pass

        # Add parameter name to the respective list, to mark its origin
        if parameter_origin == "Official":
            if parameter_name not in st.session_state["params_assumed"] and \
                    parameter_name not in st.session_state["params_calculated"]:
                if parameter_name not in st.session_state["params_official"]:
                    st.session_state["params_official"].append(parameter_name)

                # Add parameter to the parameters dictionary in the session state
                if parameter_name not in st.session_state["parameters"]:
                    st.session_state["parameters"][parameter_name] = [
                    ]
                st.session_state["parameters"][parameter_name].append(
                    parameter_value)
            else:
                st.warning(
                    "Parameter is already in assumed or calculated list")
        elif parameter_origin == "Assumed":
            if parameter_name not in st.session_state["params_official"] and \
                    parameter_name not in st.session_state["params_calculated"]:

                # expand the list of assumed parameters
                if parameter_name not in st.session_state["params_assumed"]:
                    st.session_state["params_assumed"].append(parameter_name)

                # Add parameter to the parameters dictionary in the session state
                if parameter_name not in st.session_state["parameters"]:
                    st.session_state["parameters"][parameter_name] = []
                st.session_state["parameters"][parameter_name].append(
                    parameter_value)
            else:
                st.warning(
                    "Parameter is already in official or calculated list")
        elif parameter_origin == "Calculated":
            if parameter_name not in st.session_state["params_official"] and \
                    parameter_name not in st.session_state["params_assumed"]:
                if parameter_name not in st.session_state["params_calculated"]:
                    st.session_state["params_calculated"].append(
                        parameter_name)

                # Add parameter to the parameters dictionary in the session state
                if parameter_name not in st.session_state["parameters"]:
                    st.session_state["parameters"][parameter_name] = []
                st.session_state["parameters"][parameter_name].append(
                    parameter_value)
            else:
                st.warning(
                    "Parameter is already in official or assumed list")

# st.write('st.session_state["value_name_value"]')
# st.dataframe(
#     st.session_state["value_name_value"], use_container_width=True, )


st.header("Calculate Parameters")
parameters_calculated = {}


def calculate_volume(parameters: dict) -> float:

    volume: float = parameters["length"][0] * \
        parameters["height"][0]*parameters["width"][0]
    return volume


def add_parameter_to_calculated(parameter_name: str, parameter_value_calculated=None):
    """Adds a parameter to the 'params_calculated' list in the session state if it is not already present
    in the 'params_official' or 'params_assumed' lists.
    Args:
        parameter_name (str): The name of the parameter to be added to the 'params_calculated' list.
    Raises:
        st.warning: If the parameter is already present in either the 'params_official' or 'params_assumed' lists.
    """

    if parameter_name not in st.session_state["params_official"] and \
            parameter_name not in st.session_state["params_assumed"] and \
    parameter_name not in st.session_state["params_calculated"]:
        st.session_state["params_calculated"].append(parameter_name)

        # Add parameter to the parameters dictionary in the session state
        if parameter_value_calculated is not None:
            if parameter_name not in st.session_state["parameters"]:
                st.session_state["parameters"][parameter_name] = []
            st.session_state["parameters"][parameter_name].append(
                parameter_value_calculated)
        elif parameter_name not in st.session_state["parameters"]:
            st.session_state["parameters"][parameter_name] = []
            st.session_state["parameters"][parameter_name].append(
                parameter_value)

    else:
        st.warning(
            "Parameter is already in official or assumed list")


def refresh_page():
    # st.experimental_rerun()
    st.session_state["counter"] = 0


if st.button("Calculate SI-Volume, based on depth, height and width", on_click=refresh_page()):
    volume = calculate_volume(st.session_state["value_name_value"])
    # st.session_state["value_name_value"]["volume"] = volume
    # st.write(st.session_state["value_name_value"])

    add_parameter_to_calculated("volume", parameter_value_calculated=volume)

st.divider()

col_parameters, col_param_lists = st.columns([1, 1])
with col_parameters:
    st.write('st.session_state["parameters"]')
    # st.dataframe(st.session_state["parameters"], use_container_width=True,)
    st.write(st.session_state["parameters"])

with col_param_lists:
    st.subheader("List of official Parameters")
    st.write(st.session_state["params_official"])
    st.subheader("List of Assumed Parameters")
    st.write(st.session_state["params_assumed"])
    st.subheader("List of Calculated Parameters")
    st.write(st.session_state["params_calculated"])


st.divider()
st.divider()
st.title("Summary for Backend")


def flatten_dict_simple(nested_dict: Dict[str, Any]) -> Dict[str, Any]:
    """
    Flattens a nested dictionary by bringing all nested key-value pairs to the top level.

    Args:
        nested_dict (Dict[str, Any]): The dictionary to be flattened.

    Returns:
        Dict[str, Any]: A flat dictionary with all key-value pairs from the nested dictionary.
    """
    flat_dict: Dict[str, Any] = {}
    for key, value in nested_dict.items():
        if isinstance(value, dict):
            flat_dict.update(flatten_dict_simple(value))
        else:
            flat_dict[key] = value
    return flat_dict


st.session_state["summary"] = {
    "comp_info": st.session_state["comp_info"],
    # "parameters": st.session_state["value_name_value"],
    "parameters": st.session_state["parameters"],
    "params_official": st.session_state["params_official"],
    "params_assumed": st.session_state["params_assumed"],
    "params_calculated": st.session_state["params_calculated"],
}

st.session_state["summary_flat"] = flatten_dict_simple(
    st.session_state["summary"])

# =================================================================================================
# 2024-11-26      Temporary Fix
# =================================================================================================


def add_prefix_to_keys(original_dict: Dict[str, Any]) -> Dict[str, Any]:
    """
    Parses a dictionary and adds 'i_' before every key, except for the key 'tags'.

    Args:
        original_dict (Dict[str, Any]): The dictionary whose keys need to be modified.

    Returns:
        Dict[str, Any]: A dictionary with 'i_' prefixed to every key, except for 'tags'.
    """

    exceptions = [
        "name",
        "tags",
        "shop",
        "url_picture",
        "url_shop",
        "url_datasheet",
        "url_manufacturer",
        "manufacturer",
        "params_official",
        "params_assumed",
        "params_calculated",
        "mass",
        "volume",
        "length",
        "width",
        "height",
        "data_interface",
        "description",
        "source",
        "sys_type",
        "price",
    ]


    # return {key if key == "tags" else f"i_{key}": value for key, value in original_dict.items()}
    return {key if key in exceptions else f"i_{key}": value for key, value in original_dict.items()}


st.session_state["summary_flat"] = add_prefix_to_keys(
    st.session_state["summary_flat"])

# =================================================================================================
# Temporary Fix Ende
# =================================================================================================


col_summary, col_summary_flat = st.columns([1, 1])
with col_summary:
    st.write("Structured Summary")
    st.write(st.session_state["summary"])
with col_summary_flat:
    st.write("Flattened Summary")
    st.write(st.session_state["summary_flat"])

st.divider()

def push_to_backend(summary: dict) -> dict:
    """ Displays a title and a button in the Streamlit app to save the provided summary to the backend.

    Args:
        summary (dict): A dictionary containing the summary data to be saved.

    Returns:
        dict: A dictionary containing the response from the backend (currently a demo response).
    """

    st.title("Save in Backend")
    if st.button("Save in Backend"):
        col_message, col_summary = st.columns([2, 1])
        with col_message:
            status_code, response = comp_create(summary)
            st.write("Message from the backend")
            st.write(response)

        with col_summary:
            st.write("Component summary that is pushed to the backend")
            st.write(summary)

# =================================================================================================
# =================================================================================================

response: dict = push_to_backend(st.session_state["summary_flat"])
# st.write(response)
# =================================================================================================
st.divider()
st.divider()

# st.header("Download All Entries in DecisionDB as JSON")
# st.download_button("Download JSON", get_all_decisions())

pass
