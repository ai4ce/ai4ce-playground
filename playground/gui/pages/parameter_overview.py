import pandas as pd
import plotly.express as px
import streamlit as st
from streamlit_extras.metric_cards import style_metric_cards

from playground.helpers import get_comp_statistics

st.set_page_config(
    layout="wide",
    initial_sidebar_state="collapsed",
    page_title="Parameter Overview",
    page_icon="📊",
    menu_items=None,
)

st.title("Overview of the parameters of the registered components")
st.divider()

comp_stats: dict = dict(sorted(get_comp_statistics()["components"].items()))

def count_id_occurrences(data: dict) -> dict:
    """
    Count the total occurrences of the "id" key and its occurrences per component.

    Args:
        data (dict): The dictionary containing component data.

    Returns:
        dict: A dictionary with the total count and counts per component.
    """
    total_count = 0
    component_counts = {}

    # for category, components in data.get("components", {}).items():
    for category, components in comp_stats.items():
        for component, attributes in components.items():
            if isinstance(attributes, dict):
                id_count = attributes.get("id", 0)
                component_counts[component] = id_count
                total_count += id_count

    return {"total": total_count, "per_component": component_counts}


def plot_id_pie_chart(result: dict):
    """
    Create a pie chart to visualize the occurrences of "id" per component.

    Args:
        result (dict): The dictionary containing the total and per-component counts of "id".
    """
    labels = list(result["per_component"].keys())
    values = list(result["per_component"].values())

    fig = px.pie(
        names=labels,
        values=values,
        title="Occurrences of 'id' per Component",
        hole=0.3
    )

    fig.update_traces(textinfo='percent+label')
    # fig.show()
    return fig






comps_count: dict = count_id_occurrences(comp_stats)

col_overview_metrics, col_overview_chart = st.columns([1, 2])
with col_overview_metrics:

    st.metric(
        label="Total amount of registered components",
        value=comps_count["total"], delta=comps_count["total"],
        )

    style_metric_cards(
        background_color="#FDC443",  # dark petrol
        border_color="#055471",
        border_radius_px=21,
        border_left_color="#ff00ff",
        box_shadow=False,
    )

with col_overview_chart:
    st.plotly_chart(plot_id_pie_chart(comps_count), use_container_width=True)


st.divider()
st.divider()
for key,value in comp_stats.items():

    st.header(key)

    components: dict = comp_stats.get(key, {})

    for key_comp, value_comp in components.items():
        st.subheader(key_comp)

        col_comp1, col_comp_metric, col_comp2 = st.columns([2, 1, 3], gap="small")

        # Check if the dictionary has an entry called "id"
        if "id" in components[key_comp]:
            total_amount: int = components[key_comp]["id"]

            with col_comp1:

                # Convert dictionary to a DataFrame with two columns: Parameter and Value
                battery_df = pd.DataFrame(list(components[key_comp].items()), columns=["Parameter", "Value"])
                st.dataframe(battery_df, width=440)

            with col_comp_metric:
                st.metric(
                    label="Total amount of registered components",
                    value=total_amount, delta=total_amount,
                    )
                style_metric_cards(
                    background_color="#FDC443",  # dark petrol
                    border_color="#055471",
                    border_radius_px=21,
                    border_left_color="#ff00ff",
                    box_shadow=False,
                )

            # Define a list of parameters to exclude
            exclude_params = ["name", "description", "hash", "sys_type", "manufacturer", "shop", "id", "url_manufacturer", "url_shop", "url_datasheet", "uid"]

            items = [(k, v) for k, v in components[key_comp].items() if isinstance(v, (int, float)) and k not in exclude_params]

            params = [item[0] for item in items]
            values = [item[1] for item in items]

            fig = px.bar(
                x=values,
                y=params,
                orientation='h',
                title="Battery Parameter Values",
                labels={"x": "frequency of occurrence", "y": "Parameter Name"}
            )
            fig.update_xaxes(
                dtick=1,
                range=[0, total_amount+0],
            )

            fig.add_scatter(
                x=[total_amount] * len(params),
                y=params,
                mode='lines',
                name='Total Amount',
                line=dict(color='red', dash='dash')
            )

            fig.update_layout(
                margin=dict(l=20, r=20, t=50, b=20),
                # height=600  # increase height as needed
            )
            with col_comp2:
                st.plotly_chart(fig, use_container_width=True)
    st.divider()
