from collections import OrderedDict
from typing import Any, Dict, List
import httpx
import toml
import subprocess


def add_to_dict(original_dict: dict, new_dict: dict) -> dict:
    """
    Combine two dictionaries into one.

    Args:
        original_dict (dict): The original dictionary.
        new_dict (dict): The new dictionary to be added.

    Returns:
        dict: The combined dictionary.
    """
    added_dict: dict = original_dict | new_dict
    return added_dict


def count_elements(dictionary: Dict[str, Any]) -> int:
    """
    Count the total number of elements in a dictionary, including nested dictionaries and lists.

    Args:
        dictionary (Dict[str, Any]): The dictionary to count elements in.

    Returns:
        int: The total number of elements.
    """
    total = 0
    for key, value in dictionary.items():

        if "__" not in key:
            total += 1

    return total


def fetch_toml_from_gitlab(url: str) -> dict:
    """
    Fetch a TOML file from a public GitLab repository and return its content as a dictionary.

    Args:
        url (str): The URL of the TOML file in the GitLab repository.

    Returns:
        dict: The content of the TOML file as a dictionary.
    """
    response = httpx.get(url)
    # Raise an exception if the GET request is unsuccessful.
    response.raise_for_status()

    if response.raise_for_status() == "101":
        toml_content = toml.load("playground/parameter_unit_list.toml")
    else:
        toml_content = toml.loads(response.text)
    return toml_content


def filter_key_names(data: Dict[str, Any]) -> List[str]:
    """
    Return a list of all keys in the dictionary except those that start with "__".

    Parameters:
    data (Dict[str, Any]): The input dictionary to filter.

    Returns:
    List[str]: A list of keys that do not start with "__".
    """
    return [k for k in data if not k.startswith("__")]


def filter_values_by_key(dictionary: Dict[str, Any], key: str) -> List[Any]:
    """
    Extract all unique values associated with a given key from a nested dictionary, excluding empty strings.

    Args:
        dictionary (Dict[str, Any]): The dictionary to search for the key.
        key (str): The key to search for.

    Returns:
        List[Any]: A list of unique values associated with the specified key.
    """
    values = set()

    def recursive_search(d: Dict[str, Any]):
        for k, v in d.items():
            if k == key and v != "":
                values.add(v)
            elif isinstance(v, dict):
                recursive_search(v)

    recursive_search(dictionary)
    return list(values)


def run_git_command(command: str) -> str:
    """
    Run a git command in the shell.

    Parameters:
    command (str): The git command to run.

    Returns:
    str: The output of the command or error message.
    """
    try:
        result = subprocess.run(command, shell=True,
                                check=True, text=True, capture_output=True)
        return result.stdout
    except subprocess.CalledProcessError as e:
        return f"An error occurred: {e.stderr}"


def sort_dict_alphabetically(updated_dict: dict) -> dict:
    """
    Sort a dictionary alphabetically by its keys, ignoring case.

    Args:
        updated_dict (dict): The dictionary to be sorted.

    Returns:
        dict: The sorted dictionary.
    """
    sorted_dict = OrderedDict(
        sorted(updated_dict.items(), key=lambda item: item[0].lower()))
    return sorted_dict
