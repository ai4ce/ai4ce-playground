"""
A simple function to show functionality.
"""
import logging


def calculate_orbit_velocity(planet_mass, radius):
    """Calculate the orbital velocity of an object around a planet.

    Args:
        planet_mass (float): mass of the planet in kg
        radius (float): radius of the planet in meters

    Returns:
        float: orbital velocity in m/s
    """
    g = 6.67430e-11  # gravitational constant in m^3/kg*s
    return (g * planet_mass / radius) ** 0.5


def main():
    """Calculate the orbital velocity of an object around a planet."""

    LOG_FORMAT = "%(levelname)s %(asctime)s - %(message)s"
    logging.basicConfig(
        filename="project_logs.log",
        format=LOG_FORMAT,
        filemode="w",
        level=logging.WARNING,
    )
    logger = logging.getLogger(name="General Project Logger")
    logger.warning("Starting Logging in Warning Mode.")

    m_earth = 5.9721e24  # kg
    r_earth = 6371  # km
    altitude = int(input("Input of your space asset altitude in km: "))
    print(
        "Your orbital velocity in m/s: ",
        calculate_orbit_velocity(planet_mass=m_earth, radius=(r_earth + altitude) * 1000),
    )


if __name__ == "__main__":
    main()
