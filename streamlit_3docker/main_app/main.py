import streamlit as st

st.title('Main App')

app_choice = st.sidebar.selectbox("Choose App", ["Main", "App1", "App2"])

if app_choice == "Main":
    st.write("Welcome to the main page!")
elif app_choice == "App1":
    st.markdown('<iframe src="http://localhost:8501" width="100%" height="600px"></iframe>', unsafe_allow_html=True)
elif app_choice == "App2":
    st.markdown('<iframe src="http://localhost:8502" width="100%" height="600px"></iframe>', unsafe_allow_html=True)

