import typer
import os
import subprocess

app = typer.Typer()

@app.command()
def update(
    repos_file: str = typer.Option(
        "repos.txt", help="Path to a file containing repository URLs, one per line."
    ),
    base_dir: str = typer.Option(
        None, help="Directory to clone repositories to."
    ),
):
    # Get absolute paths
    repos_file = os.path.abspath(repos_file)
    base_dir = os.path.abspath(base_dir) if base_dir else os.path.abspath(os.path.join(os.path.dirname(repos_file), ".."))
    
    # Read repository URLs from the file
    with open(repos_file, "r") as f:
        repos = f.read().splitlines()
    
    for repo_url in repos:
        repo_name = os.path.splitext(os.path.basename(repo_url))[0]
        repo_path = os.path.join(base_dir, repo_name)
    
        if os.path.exists(repo_path):
            typer.echo(f"Updating {repo_name}...")
            os.chdir(repo_path)
            subprocess.run(["git", "pull"])
        else:
            typer.echo(f"Cloning {repo_name}...")
            subprocess.run(["git", "clone", repo_url, repo_path])
    
    typer.echo("All repositories are up to date.")

if __name__ == "__main__":
    app()

