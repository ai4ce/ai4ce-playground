import streamlit as st

def app():
    st.title('App 1')
    st.write("Welcome to App 1!")

# This check allows the script to be run standalone (useful for development/testing)
if __name__ == "__main__":
    app()

