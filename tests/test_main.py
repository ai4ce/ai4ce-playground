"""
A simple test as an example
"""
import math

from hypothesis import given
from hypothesis import strategies as st
from your_project_name.main import calculate_orbit_velocity


# A pytest example
def test_calculate_orbit_velocity():
    # Test with known values for Earth
    planet_mass = 5.97219e24  # kg
    radius = 6371e3  # meters

    # compare expected value with actual value using math.isclose
    expected_velocity = 7.905363939663303e3  # m/s
    actual_velocity = calculate_orbit_velocity(planet_mass, radius)
    assert math.isclose(expected_velocity, actual_velocity, rel_tol=1e-3)

    # Test with zero values
    assert calculate_orbit_velocity(0, 100) == 0


# Testing with hypothesis
@given(semimajorAxis=st.floats().filter(lambda x: x > 0))  # Filter out values of 0 or smaller!
def test_calculate_orbit_velocity_hypothesis(semimajorAxis):
    planet_mass = 5.97219e24  # kg
    g = 6.67430e-11  # gravitational constant in m^3/kg*s

    velocity = calculate_orbit_velocity(planet_mass, semimajorAxis)
    assert math.isclose(velocity, (g * planet_mass / semimajorAxis) ** 0.5, rel_tol=1e-3)
